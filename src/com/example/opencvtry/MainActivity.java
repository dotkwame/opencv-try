package com.example.opencvtry;

import java.io.File;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {
	Button button;
	ImageView imageView;
	ImageView img2;

	File cacheDir;

	protected static final String TAG = "OpenCV";

	private BaseLoaderCallback mLoaderCallBack = new BaseLoaderCallback(this) {
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS:
				Log.i(TAG, "Open CV loaded successfully");
				break;

			default:
				super.onManagerConnected(status);
				break;
			}
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		addListenerOnButton();
		initDir();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this,
				mLoaderCallBack);

	}

	// ADDED THIS: to create/initialize external file
	// Be sure write permissions is enabled in the manifest file
	// ie add: <uses-permission android:name= "android.permission.WRITE_EXTERNAL_STORAGE"/>
	public void initDir() {
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED)) {
			cacheDir = new File(
					android.os.Environment.getExternalStorageDirectory(),
					"LazyList");

			if (!cacheDir.exists()) {
				cacheDir.mkdirs();
			}
		}
	}

	// added this to simplify creating full file path
	public String getFileAbsPath(String fileName) {
		File f = new File(cacheDir, fileName);
		return f.getAbsolutePath();
	}

	public void addListenerOnButton() {

		imageView = (ImageView) findViewById(R.id.imageView1);
		img2 = (ImageView) findViewById(R.id.imageView2);

		button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String infile = getFileAbsPath("img1.png");
				String tp = getFileAbsPath("tp.png");
				String outFile = getFileAbsPath("img2.png");

				try {
					matchTemplate(infile, tp, outFile, Imgproc.TM_CCOEFF);
					Bitmap bm = BitmapFactory.decodeFile(outFile);
					img2.setImageBitmap(bm);
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}

		});

	}

	public void matchTemplate(String inFile, String templateFile,
			String outFile, int match_method) {
		Log.i(TAG, "Running Template Matching");

		Mat img = Highgui.imread(inFile);
		Mat templ = Highgui.imread(templateFile);

		// / Create the result matrix
		int result_cols = img.cols() - templ.cols() + 1;
		int result_rows = img.rows() - templ.rows() + 1;
		Mat result = new Mat(result_rows, result_cols, CvType.CV_32FC1);

		// / Do the Matching and Normalize
		Imgproc.matchTemplate(img, templ, result, match_method);
		Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());

		// / Localizing the best match with minMaxLoc
		MinMaxLocResult mmr = Core.minMaxLoc(result);

		Point matchLoc;
		if (match_method == Imgproc.TM_SQDIFF
				|| match_method == Imgproc.TM_SQDIFF_NORMED) {
			matchLoc = mmr.minLoc;
		} else {
			matchLoc = mmr.maxLoc;
		}

		// / Show me what you got
		Core.rectangle(img, matchLoc, new Point(matchLoc.x + templ.cols(),
				matchLoc.y + templ.rows()), new Scalar(0, 255, 0));

		// Save the visualized detection.
		Log.i(TAG, "Writing: " + outFile);
		Highgui.imwrite(outFile, img);

	}
}
